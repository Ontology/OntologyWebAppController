﻿using OntologyClasses.BaseClasses;
using OntologyWebAppController.BusinessModels;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyWebAppController.BusinessFactory
{
    public class ViewMetaFactory
    {
        private clsLocalConfig localConfig;

        public List<ViewMetaItem> Views { get; set; }
        public List<clsOntologyItem> ModuleFunctions { get; set; }
        public ViewTreeNode RootNode { get; set; }

        public clsOntologyItem CreateViewList(List<clsObjectRel> ViewsToController,
            List<clsObjectRel> ViewsToModule,
            List<clsObjectRel> ModulesToModuleFunctions,
            List<clsObjectRel> ViewsToCommandLineRun)
        {
            var viewItems = (from viewToController in ViewsToController
                             join viewToModule in ViewsToModule on viewToController.ID_Object equals viewToModule.ID_Object
                             join viewToCommandLineRun in ViewsToCommandLineRun on viewToController.ID_Object equals viewToCommandLineRun.ID_Object
                             select new ViewMetaItem
                             {
                                 IdView = viewToController.ID_Object,
                                 NameView = viewToController.Name_Object,
                                 IdController = viewToController.ID_Other,
                                 NameController = viewToController.Name_Other,
                                 IdModule = viewToModule.ID_Other,
                                 NameModule = viewToModule.Name_Other,
                                 IdCommandLineRun = viewToCommandLineRun.ID_Other,
                                 NameCommandLineRun = viewToCommandLineRun.Name_Other
                             }).ToList();

            viewItems.ForEach(viewItem =>
            {
                viewItem.ModuleFunctions = ModulesToModuleFunctions.Where(moduleToFunction => moduleToFunction.ID_Object == viewItem.IdModule).Select(moduleToFunction => new clsOntologyItem
                {
                    GUID = moduleToFunction.ID_Other,
                    Name = moduleToFunction.Name_Other,
                    GUID_Parent = moduleToFunction.ID_Parent_Other,
                    Type = moduleToFunction.Ontology
                }).ToList();
            });

            Views = viewItems;

            ModuleFunctions = new List<clsOntologyItem>();
            Views.ForEach(view =>
            {
                ModuleFunctions.AddRange(view.ModuleFunctions);
            });

            ModuleFunctions = ModuleFunctions.GroupBy(moduleFunction => new { GUID = moduleFunction.GUID, Name = moduleFunction.Name, GUID_Parent = moduleFunction.GUID_Parent, Type = moduleFunction.Type }).Select(moduleFunction => new clsOntologyItem
            {
                GUID = moduleFunction.Key.GUID,
                Name = moduleFunction.Key.Name,
                GUID_Parent = moduleFunction.Key.GUID_Parent,
                Type = moduleFunction.Key.Type
            }).ToList();

            

            

            return localConfig.Globals.LState_Success.Clone();
        }

        public WebAppItem GetWebAppItem(List<clsObjectRel> entryViewData)
        {
            var urlData = entryViewData.Where(eViewRel => eViewRel.ID_Parent_Other == localConfig.OItem_class_url.GUID).FirstOrDefault();
            var pathData = entryViewData.Where(eViewRel => eViewRel.ID_Parent_Other == localConfig.OItem_class_path.GUID).FirstOrDefault();

            if (urlData == null || pathData == null) return null;

            return new WebAppItem
            {
                IdEntryView = urlData.ID_Object,
                NameEntryView = urlData.Name_Object,
                EntryUrl = urlData.Name_Other,
                EntrySessionPath = pathData.Name_Other
            };
        }

        public ViewMetaFactory(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;
        }
    }
}
