﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OntologyWebAppController.BusinessModels;
using System.Xml;
using System.IO;
using Newtonsoft.Json;
using OntoMsg_Module.Models;

namespace OntologyWebAppController.BusinessFactory
{
    public class ViewTreeFactory
    {

        private List<ViewMetaItem> viewItems;
        private List<clsOntologyItem> moduleFunctions;

        private clsLocalConfig localConfig;

        private List<BusinessModels.ViewTreeNode> nodeList;

        public List<BusinessModels.ViewTreeNode> GetViewModelTree(List<ViewMetaItem> viewItems,
            List<clsOntologyItem> moduleFunctions)
        {
            this.viewItems = viewItems;
            this.moduleFunctions = moduleFunctions;

            nodeList = new List<BusinessModels.ViewTreeNode>();
            var rootNode = new BusinessModels.ViewTreeNode
            {
                Id = localConfig.Globals.Root.GUID,
                Name = localConfig.Globals.Root.Name,
                IdPath = localConfig.Globals.Root.GUID,
                SubNodes = moduleFunctions.Select(moduleFunction => new BusinessModels.ViewTreeNode
                {
                    Id = moduleFunction.GUID,
                    Name = moduleFunction.Name,
                    IdPath = localConfig.Globals.Root.GUID + moduleFunction.GUID
                }).ToList()
            };
            nodeList.Add(rootNode);

            rootNode.SubNodes.OrderBy(node => node.Name).ToList().ForEach(moduleFunctionNode =>
            {
                moduleFunctionNode.ParentNode = rootNode;
                nodeList.Add(moduleFunctionNode);
                moduleFunctionNode.SubNodes = viewItems.Where(view => view.IsRelatedToFunction(moduleFunctionNode.Id)).Select(view => new BusinessModels.ViewTreeNode
                {
                    Id = view.IdView,
                    Name = view.NameView,
                    IdPath = moduleFunctionNode.IdPath + view.IdView,
                    ParentNode = moduleFunctionNode
                }).ToList();
                nodeList.AddRange(moduleFunctionNode.SubNodes);
            });

            

            var result = localConfig.Globals.LState_Success.Clone();

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                return nodeList;
            }
            else
            {
                return null;
            }
        }


        
        public ViewTreeFactory(clsLocalConfig localConfig)
        {
           

            this.localConfig = localConfig;


        }
        public clsOntologyItem WriteJsonDoc(StreamWriter streamWriter)
        {
            try
            {
                bool success = true;
                using (var jsonWriter = new JsonTextWriter(streamWriter))
                {
                    jsonWriter.WriteStartArray();

                    nodeList.ForEach(nodeItem =>
                    {
                        
                            jsonWriter.WriteStartObject();
                            jsonWriter.WritePropertyName("id");
                            jsonWriter.WriteValue(nodeItem.Id);
                            jsonWriter.WritePropertyName("text");
                            jsonWriter.WriteValue(nodeItem.Name);
                            jsonWriter.WritePropertyName("parentId");
                            jsonWriter.WriteValue(nodeItem.ParentNode != null ? nodeItem.ParentNode.Id : null);
                            jsonWriter.WriteEndObject();
                        
                    });
                    jsonWriter.WriteEndArray();
                }
                if (success)
                {
                    return localConfig.Globals.LState_Success.Clone();
                }
                else
                {
                    return localConfig.Globals.LState_Error.Clone();
                }

            }
            catch (Exception ex)
            {
                return localConfig.Globals.LState_Error.Clone();
            }
        }

        public clsOntologyItem WriteXMLDoc(StreamWriter streamWriter)
        {
            try
            {
                bool success = true;
                using (var xmlWriter = XmlWriter.Create(streamWriter))
                {
                    xmlWriter.WriteStartDocument();
                    xmlWriter.WriteStartElement("tree");
                    xmlWriter.WriteAttributeString("id", "0");
                    xmlWriter.WriteAttributeString("radio", "0");
                    success = nodeList.First().WriteXMLDoc(xmlWriter);
                    xmlWriter.WriteEndElement();
                }
                if (success)
                {
                    return localConfig.Globals.LState_Success.Clone();
                }
                else
                {
                    return localConfig.Globals.LState_Error.Clone();
                }
                
            }
            catch (Exception ex)
            {
                return localConfig.Globals.LState_Error.Clone();
            }

        }

        private XmlDocument GetXMLDoc()
        {
            var xmlDoc = new XmlDocument();
            var rootNode = xmlDoc.CreateElement("tree");
            var xmlAttributeNode1 = (XmlAttribute)xmlDoc.CreateNode(XmlNodeType.Attribute, "id", "");
            xmlAttributeNode1.Value = "0";
            rootNode.Attributes.Append(xmlAttributeNode1);
            var xmlAttributeNode2 = (XmlAttribute)xmlDoc.CreateNode(XmlNodeType.Attribute, "radio", "");
            xmlAttributeNode2.Value = "0";
            rootNode.Attributes.Append(xmlAttributeNode1);



            var xmlNode = nodeList.First().GetXMLForDHTMLxControl(xmlDoc);

            rootNode.AppendChild(xmlNode);
            xmlDoc.AppendChild(rootNode);

            return xmlDoc;
        }

        public string GetBase64String()
        {
            return Base64Encode("<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>" + GetXMLDoc().OuterXml);
        }

        private string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }
    }
}
