﻿using OntologyClasses.DataClasses;
using OntologyWebAppController.Notifications;
using OntoMsg_Module.Services;
using OntoMsg_Module.WebSocketServices;
using System;
using System.Collections.Generic;
using System.Linq;
using WebSocketSharp;
using System.Text;
using System.Threading.Tasks;
using OntoMsg_Module.Base;
using OntoMsg_Module;
using System.Runtime.InteropServices;
using System.Reflection;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Notifications;
using OntologyWebAppController.Services;
using OntologyWebAppController.BusinessModels;
using OntologyWebAppController.BusinessFactory;
using OntologyWebAppController.Translations;
using OntoMsg_Module.Models;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.StateMachines;
using System.ComponentModel;

namespace OntologyWebAppController.Controllers
{
    public class NavigationViewController : NavigationViewModel, IViewController
    {
        private WebsocketServiceAgent webSocketServiceAgent;
        private clsLocalConfig localConfig;
        private ServiceAgent_Elastic serviceAgentElastic;
        private ViewMetaFactory viewMetaFactory;
        private ViewTreeFactory viewTreeFactory;
        private List<ViewMetaItem> views;

        private TranslationController translationController = new TranslationController();

        private clsOntologyItem oItemUser;
        private clsOntologyItem oItemGroup;

        private clsLogStates logStates = new clsLogStates();

        private string treeFileName;
        private int panelId = 0;

        public IControllerStateMachine StateMachine { get; private set; }
        public ControllerStateMachine LocalStateMachine
        {
            get
            {
                if (StateMachine == null)
                {
                    return null;
                }

                return (ControllerStateMachine)StateMachine;
            }
        }

        public NavigationViewController()
        {
            
            // local configuration
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            Initialize();
            PropertyChanged += NavigationViewController_PropertyChanged;
        }

        private void NavigationViewController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var property = ViewModelProperties.FirstOrDefault(viewProperty => viewProperty.Property.Name == e.PropertyName);

            if (property == null) return;

            property.ViewItem.AddValue(property.Property.GetValue(this));

            if (e.PropertyName == Notifications.NotifyChanges.Navigation_IsSuccessful_Login)
            {
                webSocketServiceAgent.SendPropertyChange(e.PropertyName);
            }
           else if (e.PropertyName == Notifications.NotifyChanges.Navigation_Url_TreeData)
            {
                webSocketServiceAgent.SendPropertyChange(e.PropertyName);
            }
           else if (e.PropertyName == Notifications.NotifyChanges.Navigation_Text_SelectedNodeId)
            {
                var selectedView = views.FirstOrDefault(viewItem => viewItem.IdView == Text_SelectedNodeId);
                if (selectedView == null) return;

                Navigate(selectedView);
  

                
            }
            else if (e.PropertyName == Notifications.NotifyChanges.Navigation_Url_ViewNavigation ||
                e.PropertyName == Notifications.NotifyChanges.Navigation_Tab_Init ||
                e.PropertyName == Notifications.NotifyChanges.Navigation_NewTabDivItem ||
                e.PropertyName == Notifications.NotifyChanges.Navigation_NewNavigationItem ||
                e.PropertyName == Notifications.NotifyChanges.Navigation_SplitterMode ||
                e.PropertyName == Notifications.NotifyChanges.View_IsEnabled_Four ||
                e.PropertyName == Notifications.NotifyChanges.View_IsEnabled_Horizontal ||
                e.PropertyName == Notifications.NotifyChanges.View_IsEnabled_Vertical)
            {
                webSocketServiceAgent.SendPropertyChange(e.PropertyName);
            }
           else if (e.PropertyName == Notifications.NotifyChanges.View_IsToggled_Horizontal)
            {
                if (IsToggled_Horizontal)
                {
                    IsToggled_Vertical = IsToggled_Four = false;
                    IsEnabled_Vertical = IsEnabled_Four = false;
                    SetSplitterMode();
                }
                
            }
            else if (e.PropertyName == Notifications.NotifyChanges.View_IsToggled_Vertical)
            {
                if (IsToggled_Vertical)
                {
                    IsToggled_Horizontal = IsToggled_Four = false;
                    IsEnabled_Horizontal = IsEnabled_Four = false;
                    SetSplitterMode();
                }
                
            }
            else if (e.PropertyName == Notifications.NotifyChanges.View_IsToggled_Four)
            {
                if (IsToggled_Four)
                {
                    IsToggled_Vertical = IsToggled_Horizontal = false;
                    IsEnabled_Horizontal = IsEnabled_Vertical = false;
                    SetSplitterMode();
                }
                
            }

        }

        private void Navigate(ViewMetaItem selectedView, string classId = null, string objectId = null, string relationTypeId = null, string attributeTypeId = null)
        {
            var navigation = OntoMsg_Module.ModuleDataExchanger.CurrentWebApp.EntryUrl + (OntoMsg_Module.ModuleDataExchanger.CurrentWebApp.EntryUrl.EndsWith("/") ? "" : "/") + selectedView.NameCommandLineRun;

            var paramList = "";
            if (!string.IsNullOrEmpty(classId))
            {
                paramList += "&Class=" + classId;
            }

            if (!string.IsNullOrEmpty(objectId))
            {
                paramList += "&Object=" + objectId;
            }

            if (!string.IsNullOrEmpty(relationTypeId))
            {
                paramList += "&RelationType=" + relationTypeId;
            }

            if (!string.IsNullOrEmpty(attributeTypeId))
            {
                paramList += "&AttributeType=" + attributeTypeId;
            }

            if (!string.IsNullOrEmpty(paramList))
            {
                paramList = "?" + paramList.Substring(1);
                navigation += paramList;
            }

            

            var container = "";
            var itemType = IFrameItemType.NewWindow;
            if (IsToggled_Horizontal || IsToggled_Vertical || IsToggled_Four)
            {
                if (IsToggled_Horizontal || IsToggled_Vertical)
                {
                    itemType = IsToggled_OpenWindow ? IFrameItemType.NewWindow :
                        panelId < 2 ? IFrameItemType.Panel : IFrameItemType.NewWindow;
                    container = panelId == 0 ? "left" : "right";
                }
                else
                {
                    itemType = IsToggled_OpenWindow ? IFrameItemType.NewWindow :
                        panelId < 4 ? IFrameItemType.Panel : IFrameItemType.NewWindow;

                    switch (panelId)
                    {
                        case 0:
                            container = "one";
                            break;
                        case 1:
                            container = "two";
                            break;
                        case 2:
                            container = "three";
                            break;
                        case 3:
                            container = "four";
                            break;
                    }

                }
            }
            else if (!IsToggled_OpenWindow)
            {
                itemType = IFrameItemType.TabPage;
                container = "content";

            }

            var iframeItem = new IFrameItem
            {
                Id = "ifrm_" + selectedView.IdView,
                Name = selectedView.NameView,
                src = navigation,
                ItemType = itemType,
                ContainerId = container
            };
            NewNavigationItem = iframeItem;
            panelId++;
        }

        private void SetSplitterMode()
        {
            SplitterMode = IsToggled_Horizontal ? SplitterMode.Horizontal : IsToggled_Vertical ? SplitterMode.Vertical : IsToggled_Four ? SplitterMode.Four : SplitterMode.None;
        }
        

        private void WebSocketServiceAgent_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_DataText_SessionId)
            {
                if (!string.IsNullOrEmpty(Url_TreeData))
                {
                    RaisePropertyChanged(Notifications.NotifyChanges.Navigation_Url_TreeData);
                }
                

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Command_RequestedCommand)
            {


            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Event_RequestEvent)
            {

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_ChangedProperty)
            {
                if (webSocketServiceAgent.ChangedProperty.Key == Notifications.NotifyChanges.Navigation_Text_SelectedNodeId)
                {
                    Text_SelectedNodeId = webSocketServiceAgent.ChangedProperty.Value.ToString();
                }
                else if (webSocketServiceAgent.ChangedProperty.Key == Notifications.NotifyChanges.View_IsToggled_OpenWindow)
                {
                    bool checkValue;

                    if (bool.TryParse(webSocketServiceAgent.ChangedProperty.Value.ToString(), out checkValue))
                    {
                        IsToggled_OpenWindow = checkValue;
                    }
                   
                }
                else if (webSocketServiceAgent.ChangedProperty.Key == Notifications.NotifyChanges.View_IsToggled_Vertical)
                {
                    bool checkValue;

                    if (bool.TryParse(webSocketServiceAgent.ChangedProperty.Value.ToString(), out checkValue))
                    {
                        IsToggled_Vertical = checkValue;
                    }
                   
                }
                else if (webSocketServiceAgent.ChangedProperty.Key == Notifications.NotifyChanges.View_IsToggled_Horizontal)
                {
                    bool checkValue;

                    if (bool.TryParse(webSocketServiceAgent.ChangedProperty.Value.ToString(), out checkValue))
                    {
                        IsToggled_Horizontal = checkValue;
                    }

                }
                else if (webSocketServiceAgent.ChangedProperty.Key == Notifications.NotifyChanges.View_IsToggled_Four)
                {
                    bool checkValue;

                    if (bool.TryParse(webSocketServiceAgent.ChangedProperty.Value.ToString(), out checkValue))
                    {
                        IsToggled_Four = checkValue;
                    }

                }
            }
            
           
        }

        
        private void Initialize()
        {
            var stateMachine = new ControllerStateMachine(StateMachineType.BlockSelectingSelected | StateMachineType.BlockLoadingSelected);
            stateMachine.IsControllerListen = true;
            StateMachine = stateMachine;
            stateMachine.PropertyChanged += StateMachine_PropertyChanged;
            stateMachine.loadSelectedItem += StateMachine_loadSelectedItem;
            stateMachine.loginSucceded += StateMachine_loginSucceded;
            stateMachine.openedSocket += StateMachine_openedSocket;
            stateMachine.closedSocket += StateMachine_closedSocket;

            serviceAgentElastic = new ServiceAgent_Elastic(localConfig);
            serviceAgentElastic.PropertyChanged += ServiceAgentElastic_PropertyChanged;
            viewMetaFactory = new ViewMetaFactory(localConfig);
            viewTreeFactory = new ViewTreeFactory(localConfig);


        }

        private void StateMachine_closedSocket()
        {
            webSocketServiceAgent.RemoveAllResources();

            viewMetaFactory = null;
            views = null;
            serviceAgentElastic = null;
        }

        private void StateMachine_openedSocket()
        {
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Sender,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });
        }

        private void StateMachine_loginSucceded()
        {
           
            var result = serviceAgentElastic.GetData();
            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {

            }
           
            
        }

        private void StateMachine_loadSelectedItem(clsOntologyItem oItemSelected)
        {
            
        }

        private void StateMachine_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(OntoMsg_Module.StateMachines.ControllerStateMachine.LoginSuccessful))
            {
                IsSuccessful_Login = LocalStateMachine.LoginSuccessful;
            }
        }

        public void InitializeViewController(WebsocketServiceAgent webSocketServiceAgent)
        {
            this.webSocketServiceAgent = webSocketServiceAgent;
            this.webSocketServiceAgent.PropertyChanged += WebSocketServiceAgent_PropertyChanged;
            this.webSocketServiceAgent.comServerOnMessage += WebSocketServiceAgent_comServerOnMessage;
            this.webSocketServiceAgent.comServerOpened += WebSocketServiceAgent_comServerOpened;
            
        }

       
        private void WebSocketServiceAgent_comServerOpened()
        {
            var message = new InterServiceMessage
            {
                ChannelId = Channels.Login,
                SenderId = webSocketServiceAgent.EndpointId
            };

            webSocketServiceAgent.SendInterModMessage(message);
        }

        private void WebSocketServiceAgent_comServerOnMessage(OntoMsg_Module.Notifications.InterServiceMessage message)
        {
            if (message.ReceiverId != null && message.ReceiverId != webSocketServiceAgent.EndpointId) return;
            
        }

        private void ServiceAgentElastic_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == Notifications.NotifyChanges.Elastic_ResultData)
            {
                if (serviceAgentElastic == null || serviceAgentElastic.ResultData == null) return;

                if (serviceAgentElastic.ResultData.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    return;
                    
                        
                }

                var result = viewMetaFactory.CreateViewList(serviceAgentElastic.ViewsToController,
                        serviceAgentElastic.ViewsToModules,
                        serviceAgentElastic.ModulesToModuleFunctions,
                        serviceAgentElastic.ViewsToCommandLineRun);

                if (result.GUID == localConfig.Globals.LState_Success.GUID)
                {

                    views = viewMetaFactory.Views;
                    ModuleDataExchanger.ClearViews();
                    views.ForEach(viewItem =>
                    {
                        ModuleDataExchanger.AddView(viewItem);
                    });

                    var treeList = viewTreeFactory.GetViewModelTree(viewMetaFactory.Views.OrderBy(view => view.NameView).ToList(), viewMetaFactory.ModuleFunctions).OrderBy(treeItem => treeItem.Name).ToList();
                    if (treeList != null)
                    {
                        treeFileName = Guid.NewGuid().ToString() + ".json";

                        var sessionFile = webSocketServiceAgent.RequestWriteStream(treeFileName);
                        if (sessionFile.FileUri == null)
                        {
                            Message_FormMessage = new ViewMessage(translationController.Text_WebConfigError, translationController.Title_WebConfigError, ViewMessageType.Critical | ViewMessageType.OK);
                            IsActive_LoadProgressState = false;
                            return;
                        }

                        if (sessionFile.StreamWriter == null)
                        {
                            Message_FormMessage = new ViewMessage(translationController.Text_FileStreamError, translationController.Title_FileStreamError, ViewMessageType.Critical | ViewMessageType.OK);
                            IsActive_LoadProgressState = false;
                            return;
                        }

                        using (sessionFile.StreamWriter)
                        {
                            result = viewTreeFactory.WriteJsonDoc(sessionFile.StreamWriter);

                        }
                        if (result.GUID == localConfig.Globals.LState_Error.GUID)
                        {
                            Message_FormMessage = new ViewMessage(translationController.Text_FileStreamError, translationController.Title_FileStreamError, ViewMessageType.Critical | ViewMessageType.OK);
                            IsActive_LoadProgressState = false;
                            return;
                        }

                        
                        Url_TreeData = sessionFile.FileUri.AbsoluteUri;
                    }

                    if (!string.IsNullOrEmpty(DataText_ViewId))
                    {
                        var view = ModuleDataExchanger.GetViewById(DataText_ViewId);

                        if (view != null)
                        {
                            Navigate(view, DataText_ClassId, DataText_ObjectId, DataText_RelationTypeId, DataText_AttributeTypeId);
                        }
                    }
                }
            }
        }

        public bool ControllerFunctionIsChecked()
        {
            return false;
        }


        public List<ViewModelProperty> GetViewModelProperties(bool onlySend = true, ViewItemType viewItemType = ViewItemType.All, ViewItemClass viewItemClass = ViewItemClass.All)
        {
            return ViewModelProperties.Where(viewItemProp => (onlySend ? viewItemProp.ViewModelAttribute.Send : 1 == 1)
                && (viewItemType != ViewItemType.All ? viewItemProp.ViewModelAttribute.ViewItemType == viewItemType : 1 == 1)
                && (viewItemClass != ViewItemClass.All ? viewItemProp.ViewModelAttribute.ViewItemClass == viewItemClass : 1 == 1)).ToList();
        }
    }
}
