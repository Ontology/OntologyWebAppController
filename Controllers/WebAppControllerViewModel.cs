﻿using OntologyWebAppController.Notifications;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyWebAppController.Controllers
{
    public class WebAppControllerViewModel : OntoMsg_Module.Base.ViewModelBase
    {
        private string text_UserName;
        [ViewModel(Send = true)]
        public string Text_UserName
        {
            get { return text_UserName; }
            set
            {

                text_UserName = value;

                RaisePropertyChanged(NotifyChanges.View_Text_UserName);

            }
        }

        private string text_Password;
        [ViewModel(Send = true)]
        public string Text_Password
        {
            get { return text_Password; }
            set
            {

                text_Password = value;

                RaisePropertyChanged(NotifyChanges.View_Text_Password);

            }
        }

        private string text_Group;
        [ViewModel(Send = true)]
        public string Text_Group
        {
            get { return text_Group; }
            set
            {

                text_Group = value;

                RaisePropertyChanged(NotifyChanges.View_Text_Group);

            }
        }

        private string text_Error;
        [ViewModel(Send = true)]
        public string Text_Error
        {
            get { return text_Error; }
            set
            {

                text_Error = value;

                RaisePropertyChanged(NotifyChanges.View_Text_Error);

            }
        }

        private string url_Navigation;
        [ViewModel(Send = true)]
        public string Url_Navigation
        {
            get { return url_Navigation; }
            set
            {
                if (url_Navigation == value) return;

                url_Navigation = value;

                RaisePropertyChanged(NotifyChanges.View_Url_Navigation);

            }
        }

        private string sessionId;
        [ViewModel(Send = true)]
        public string SessionId
        {
            get { return sessionId; }
            set
            {

                if (sessionId == value) return;
                sessionId = value;

                RaisePropertyChanged("SessionId");

            }
        }

        private string text_Cookie;
        [ViewModel(Send = true)]
        public string Text_Cookie
        {
            get
            {
                return text_Cookie;
            }
            set
            {
                text_Cookie = value;
                RaisePropertyChanged(NotifyChanges.ViewMode_Text_Cookie);
            }
        }

        private DateTime? dateTimeStamp_Expire;
        [ViewModel(Send = true)]
        public DateTime? DateTimeStamp_Expire
        {
            get { return dateTimeStamp_Expire; }
            set
            {

                if (dateTimeStamp_Expire == value) return;
                dateTimeStamp_Expire = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DateTimeStamp_Expire);

            }
        }

        private string datatext_AttributeTypeId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "attributeTypeId", ViewItemType = ViewItemType.Other)]
        public string DataText_AttributeTypeId
        {
            get { return datatext_AttributeTypeId; }
            set
            {
                if (datatext_AttributeTypeId == value) return;

                datatext_AttributeTypeId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_AttributeTypeId);

            }
        }

        private string datatext_RelationTypeId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "relationTypeId", ViewItemType = ViewItemType.Other)]
        public string DataText_RelationTypeId
        {
            get { return datatext_RelationTypeId; }
            set
            {
                if (datatext_RelationTypeId == value) return;

                datatext_RelationTypeId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_RelationTypeId);

            }
        }

        private string datatext_ObjectId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "objectId", ViewItemType = ViewItemType.Other)]
        public string DataText_ObjectId
        {
            get { return datatext_ObjectId; }
            set
            {
                if (datatext_ObjectId == value) return;

                datatext_ObjectId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_ObjectId);

            }
        }

        private string datatext_ClassId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "classId", ViewItemType = ViewItemType.Other)]
        public string DataText_ClassId
        {
            get { return datatext_ClassId; }
            set
            {
                if (datatext_ClassId == value) return;

                datatext_ClassId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_ClassId);

            }
        }

        private string datatext_ViewId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "viewId", ViewItemType = ViewItemType.Other)]
        public string DataText_ViewId
        {
            get { return datatext_ViewId; }
            set
            {
                if (datatext_ViewId == value) return;

                datatext_ViewId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_ViewId);

            }
        }

    }
}
