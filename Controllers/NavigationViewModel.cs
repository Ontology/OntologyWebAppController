﻿using OntologyWebAppController.BusinessModels;
using OntologyWebAppController.Notifications;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyWebAppController.Controllers
{
    public enum SplitterMode
    {
        None = 0,
        Vertical = 1,
        Horizontal = 2,
        Four = 3
    }
    public class NavigationViewModel : OntoMsg_Module.Base.ViewModelBase
    {
        private bool issuccessful_Login;
        [ViewModel(Send = true)]
        public bool IsSuccessful_Login
        {
            get { return issuccessful_Login; }
            set
            {

                issuccessful_Login = value;

                RaisePropertyChanged(NotifyChanges.Navigation_IsSuccessful_Login);

            }
        }

        private string url_TreeData;
        [ViewModel(Send = true)]
        public string Url_TreeData
        {
            get { return url_TreeData; }
            set
            {

                url_TreeData = value;

                RaisePropertyChanged(NotifyChanges.Navigation_Url_TreeData);

            }
        }

        private ViewMessage message_FormMessage;
        [ViewModel(Send = true)]
        public ViewMessage Message_FormMessage
        {
            get { return message_FormMessage; }
            set
            {
                if (message_FormMessage == value) return;

                message_FormMessage = value;

                RaisePropertyChanged(NotifyChanges.View_Message_FormMessage);

            }
        }

        private bool isactive_LoadProgressState;
        [ViewModel(Send = true)]
        public bool IsActive_LoadProgressState
        {
            get { return isactive_LoadProgressState; }
            set
            {
                if (isactive_LoadProgressState == value) return;

                isactive_LoadProgressState = value;

                RaisePropertyChanged(NotifyChanges.View_IsActive_LoadProgressState);

            }
        }

        private bool isToggled_OpenWindow;
        [ViewModel(Send = true)]
        public bool IsToggled_OpenWindow
        {
            get { return isToggled_OpenWindow; }
            set
            {
                if (isToggled_OpenWindow == value) return;

                isToggled_OpenWindow = value;

                RaisePropertyChanged(NotifyChanges.View_IsToggled_OpenWindow);

            }
        }

        private bool isEnabled_Vertical = true;
        [ViewModel(Send = true)]
        public bool IsEnabled_Vertical
        {
            get { return isEnabled_Vertical; }
            set
            {
                if (isEnabled_Vertical == value) return;

                isEnabled_Vertical = value;

                RaisePropertyChanged(NotifyChanges.View_IsEnabled_Vertical);

            }
        }

        private bool isToggled_Vertical;
        [ViewModel(Send = true)]
        public bool IsToggled_Vertical
        {
            get { return isToggled_Vertical; }
            set
            {
                if (isToggled_Vertical == value) return;

                isToggled_Vertical = value;

                RaisePropertyChanged(NotifyChanges.View_IsToggled_Vertical);

            }
        }

        private bool isEnabled_Horizontal = true;
        [ViewModel(Send = true)]
        public bool IsEnabled_Horizontal
        {
            get { return isEnabled_Horizontal; }
            set
            {
                if (isEnabled_Horizontal == value) return;

                isEnabled_Horizontal = value;

                RaisePropertyChanged(NotifyChanges.View_IsEnabled_Horizontal);

            }
        }

        private bool isToggled_Horizontal;
        [ViewModel(Send = true)]
        public bool IsToggled_Horizontal
        {
            get { return isToggled_Horizontal; }
            set
            {
                if (isToggled_Horizontal == value) return;

                isToggled_Horizontal = value;

                RaisePropertyChanged(NotifyChanges.View_IsToggled_Horizontal);

            }
        }

        private bool isEnabled_Four = true;
        [ViewModel(Send = true)]
        public bool IsEnabled_Four
        {
            get { return isEnabled_Four; }
            set
            {
                if (isEnabled_Four == value) return;

                isEnabled_Four = value;

                RaisePropertyChanged(NotifyChanges.View_IsEnabled_Four);

            }
        }

        private bool isToggled_Four;
        [ViewModel(Send = true)]
        public bool IsToggled_Four
        {
            get { return isToggled_Four; }
            set
            {
                if (isToggled_Four == value) return;

                isToggled_Four = value;

                RaisePropertyChanged(NotifyChanges.View_IsToggled_Four);

            }
        }

        private string text_SelectedNodeId;
        [ViewModel(Send = true)]
        public string Text_SelectedNodeId
        {
            get { return text_SelectedNodeId; }
            set
            {

                text_SelectedNodeId = value;

                RaisePropertyChanged(NotifyChanges.Navigation_Text_SelectedNodeId);

            }
        }

        private string url_ViewNavigation;
        [ViewModel(Send = true)]
        public string Url_ViewNavigation
        {
            get { return url_ViewNavigation; }
            set
            {

                url_ViewNavigation = value;

                RaisePropertyChanged(NotifyChanges.Navigation_Url_ViewNavigation);

            }
        }

        private UnsortedList tabInit;
        [ViewModel(Send = true)]
        public UnsortedList Tab_Init
        {
            get { return tabInit; }
            set
            {

                if (tabInit == value) return;
                tabInit = value;

                RaisePropertyChanged(NotifyChanges.Navigation_Tab_Init);

            }
        }

        private IFrameItem newNavigationItem;
        [ViewModel(Send = true)]
        public IFrameItem NewNavigationItem
        {
            get { return newNavigationItem; }
            set
            {

                if (newNavigationItem == value) return;
                newNavigationItem = value;

                RaisePropertyChanged(NotifyChanges.Navigation_NewNavigationItem);

            }
        }

        private IframeDivItem newTabDivItem;
        [ViewModel(Send = true)]
        public IframeDivItem NewTabDivItem
        {
            get { return newTabDivItem; }
            set
            {

                if (newTabDivItem == value) return;
                newTabDivItem = value;

                RaisePropertyChanged(NotifyChanges.Navigation_NewTabDivItem);

            }
        }

        private SplitterMode splitterMode;
        [ViewModel(Send = true)]
        public SplitterMode SplitterMode
        {
            get { return splitterMode; }
            set
            {

                if (splitterMode == value) return;
                splitterMode = value;

                RaisePropertyChanged(NotifyChanges.Navigation_SplitterMode);

            }
        }

        private string datatext_AttributeTypeId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "attributeTypeId", ViewItemType = ViewItemType.Other)]
        public string DataText_AttributeTypeId
        {
            get { return datatext_AttributeTypeId; }
            set
            {
                if (datatext_AttributeTypeId == value) return;

                datatext_AttributeTypeId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_AttributeTypeId);

            }
        }

        private string datatext_RelationTypeId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "relationTypeId", ViewItemType = ViewItemType.Other)]
        public string DataText_RelationTypeId
        {
            get { return datatext_RelationTypeId; }
            set
            {
                if (datatext_RelationTypeId == value) return;

                datatext_RelationTypeId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_RelationTypeId);

            }
        }

        private string datatext_ObjectId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "objectId", ViewItemType = ViewItemType.Other)]
        public string DataText_ObjectId
        {
            get { return datatext_ObjectId; }
            set
            {
                if (datatext_ObjectId == value) return;

                datatext_ObjectId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_ObjectId);

            }
        }

        private string datatext_ClassId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "classId", ViewItemType = ViewItemType.Other)]
        public string DataText_ClassId
        {
            get { return datatext_ClassId; }
            set
            {
                if (datatext_ClassId == value) return;

                datatext_ClassId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_ClassId);

            }
        }

        private string datatext_ViewId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "viewId", ViewItemType = ViewItemType.Other)]
        public string DataText_ViewId
        {
            get { return datatext_ViewId; }
            set
            {
                if (datatext_ViewId == value) return;

                datatext_ViewId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_ViewId);

            }
        }

    }
}
