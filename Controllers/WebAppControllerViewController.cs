﻿using OntologyWebAppController.Notifications;
using OntoMsg_Module.Base;
using OntoMsg_Module.Services;
using OntoMsg_Module.WebSocketServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SecurityModule;
using OntologyAppDBConnector;
using OntologyClasses.DataClasses;
using OntoMsg_Module;
using System.Runtime.InteropServices;
using System.Reflection;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Notifications;
using OntologyWebAppController.Services;
using OntologyWebAppController.BusinessFactory;
using OntoMsg_Module.Models;
using OntoMsg_Module.Attributes;
using Newtonsoft.Json.Linq;
using OntoMsg_Module.StateMachines;
using System.ComponentModel;

namespace OntologyWebAppController.Controllers
{
    public class WebAppControllerViewController : WebAppControllerViewModel, IViewController
    {
        private WebsocketServiceAgent webSocketServiceAgent;
        private FileSystemServiceAgent fileSystemServiceAgent;
        private ServiceAgent_Elastic serviceAgentElastic;
        private ViewMetaFactory viewMetaFactory;

        private clsLocalConfig localConfig;
        private clsOntologyItem oItemUser;
        private clsOntologyItem oItemGroup;

        private bool isUserSender;

        private static SecurityController securityController;
        private clsLogStates logStates = new clsLogStates();

        public IControllerStateMachine StateMachine { get; private set; }
        public ControllerStateMachine LocalStateMachine
        {
            get
            {
                if (StateMachine == null)
                {
                    return null;
                }

                return (ControllerStateMachine)StateMachine;
            }
        }

        public WebAppControllerViewController()
        {
            // local configuration
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }
            

            
            Initialize();
            PropertyChanged += WebAppControllerViewController_PropertyChanged; ;
        }

        private void WebAppControllerViewController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var property = ViewModelProperties.FirstOrDefault(viewProperty => viewProperty.Property.Name == e.PropertyName);

            if (property == null) return;

            property.ViewItem.AddValue(property.Property.GetValue(this));

            if (e.PropertyName == Notifications.NotifyChanges.View_Text_Error)
            {
                webSocketServiceAgent.SendPropertyChange(Notifications.NotifyChanges.View_Text_Error);
            }
            else if (e.PropertyName == Notifications.NotifyChanges.View_Url_Navigation)
            {
                webSocketServiceAgent.SendPropertyChange(Notifications.NotifyChanges.View_Url_Navigation);
            }
            else if (e.PropertyName == Notifications.NotifyChanges.ViewModel_DateTimeStamp_Expire ||
                e.PropertyName == Notifications.NotifyChanges.View_Text_UserName ||
                e.PropertyName == Notifications.NotifyChanges.View_Text_Group ||
                e.PropertyName == Notifications.NotifyChanges.View_Text_Password ||
                e.PropertyName == Notifications.NotifyChanges.ViewMode_Text_Cookie)
            {
                webSocketServiceAgent.SendPropertyChange(e.PropertyName);
            }
        }

        private void WebSocketServiceAgent_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_DataText_SessionId)
            {
                
                
            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Command_RequestedCommand)
            {
                var textUserName = "";
                var textGroup = "";
                var textPassword = "";

                var command = webSocketServiceAgent.Command_RequestedCommand;
                if (command == Notifications.NotifyChanges.Command_Login)
                {
                    

                    if (webSocketServiceAgent.Request.ContainsKey(Notifications.NotifyChanges.View_Text_UserName))
                    {
                        textUserName = webSocketServiceAgent.Request[Notifications.NotifyChanges.View_Text_UserName].ToString();
                    }
                    if (webSocketServiceAgent.Request.ContainsKey(Notifications.NotifyChanges.View_Text_Group))
                    {
                        textGroup = webSocketServiceAgent.Request[Notifications.NotifyChanges.View_Text_Group].ToString();
                    }
                    if (webSocketServiceAgent.Request.ContainsKey(Notifications.NotifyChanges.View_Text_Password))
                    {
                        textPassword = webSocketServiceAgent.Request[Notifications.NotifyChanges.View_Text_Password].ToString();
                    }
                    if (webSocketServiceAgent.Request.ContainsKey(Notifications.NotifyChanges.View_IsUserSender))
                    {
                        if (webSocketServiceAgent.Request[Notifications.NotifyChanges.View_IsUserSender].ToString().ToLower() == "on")
                        {
                            isUserSender = true;
                        }
                        
                    }

                    DateTimeStamp_Expire = DateTime.UtcNow.AddDays(365);

                    if (IsValid(textUserName, textGroup, textPassword))
                    {
                        SessionId = webSocketServiceAgent.DataText_SessionId;
                        Text_Cookie = "session=" + SessionId + ";expires=" + DateTimeStamp_Expire.Value.ToUniversalTime().ToString("r");

                        var dictCredentials = new Dictionary<string, object>();

                        dictCredentials.Add(WebSocketBase.QueryParam_Session, webSocketServiceAgent.DataText_SessionId);
                        dictCredentials.Add(Notifications.NotifyChanges.View_Text_UserName, textUserName);
                        dictCredentials.Add(Notifications.NotifyChanges.View_Text_Group, textGroup);
                        dictCredentials.Add(Notifications.NotifyChanges.View_Text_Password, textPassword);
                        dictCredentials.Add(Notifications.NotifyChanges.View_IsUserSender, isUserSender);
                        dictCredentials.Add(Notifications.NotifyChanges.ViewModel_DateTimeStamp_Expire, DateTimeStamp_Expire);

                        var userDoc = new clsAppDocuments { Id = webSocketServiceAgent.DataText_SessionId, Dict = dictCredentials };
                        serviceAgentElastic.UserDbUpdater.SaveDoc(new List<clsAppDocuments> { userDoc }, strType: "session");

                        Url_Navigation = ModuleDataExchanger.CurrentWebApp.EntryUrl + "/Navigation/Navigation.html?Session=" + webSocketServiceAgent.DataText_SessionId;
                    }
                    else
                    {
                        Text_Error = "Credentials wrong.";
                    }
                }

                
                
            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Event_RequestEvent)
            {
                
            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_ChangedProperty)
            {
                if (webSocketServiceAgent.ChangedProperty.Key == Notifications.NotifyChanges.View_Text_UserName)
                {
                    Text_UserName = webSocketServiceAgent.ChangedProperty.Value.ToString() ;
                }
                else if (webSocketServiceAgent.ChangedProperty.Key == Notifications.NotifyChanges.View_Text_Password)
                {
                    Text_Password = webSocketServiceAgent.ChangedProperty.Value.ToString();
                }
                else if (webSocketServiceAgent.ChangedProperty.Key == Notifications.NotifyChanges.View_Text_Group)
                {
                    Text_Group = webSocketServiceAgent.ChangedProperty.Value.ToString();
                }
                else if (webSocketServiceAgent.ChangedProperty.Key == Notifications.NotifyChanges.ViewModel_DateTimeStamp_Expire)
                {
                 
                }
                else if (webSocketServiceAgent.ChangedProperty.Key == Notifications.NotifyChanges.ViewMode_Text_Cookie)
                {
                    var cookieValues = webSocketServiceAgent.ChangedProperty.Value.ToString().Split(';');
                    var sessionValue = cookieValues.FirstOrDefault(cookieValue => cookieValue.Contains("session="));

                    if (!string.IsNullOrEmpty(sessionValue))
                    {
                        var sessionId = sessionValue.Substring(sessionValue.IndexOf('=') + 1);
                        sessionId = sessionId.Trim(); 
                        if (CheckAuthentication(sessionId))
                        {
                            SessionId = sessionId;
                            webSocketServiceAgent.DataText_SessionId = sessionId;

                            var url = ModuleDataExchanger.CurrentWebApp.EntryUrl + "/Navigation/Navigation.html?Session=" + webSocketServiceAgent.DataText_SessionId;
                            if (!string.IsNullOrEmpty(DataText_ViewId))
                            {
                                url += "&View=" + DataText_ViewId;
                                
                            }
                            
                            if (!string.IsNullOrEmpty(DataText_ClassId))
                            {
                                url += "&Class=" + DataText_ClassId;
                            }

                            if (!string.IsNullOrEmpty(DataText_ObjectId))
                            {
                                url += "&Object=" + DataText_ObjectId;
                            }

                            if (!string.IsNullOrEmpty(DataText_RelationTypeId))
                            {
                                url += "&RelationType=" + DataText_RelationTypeId;
                            }

                            if (!string.IsNullOrEmpty(DataText_AttributeTypeId))
                            {
                                url += "&AttributeType=" + DataText_RelationTypeId;
                            }


                            Url_Navigation = url;

                        }
                    }
                }
            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_ChangedViewItems)
            {
                webSocketServiceAgent.ChangedViewItems.ForEach(changedItem =>
                {
                   
                });
            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_ViewArguments)
            {

            }
        }

        private bool IsValid(string textUserName, string textGroup, string textPassword)
        {
            if (!securityController.IsInitialized)
            {
                oItemUser = securityController.GetUser(textUserName);
                oItemGroup = securityController.GetGroup(textGroup);

                if (oItemUser == null || oItemGroup == null)
                {

                    return false;
                }

                var result = securityController.InitializeUser(oItemUser, textPassword);
                if (result.GUID != logStates.LogState_Success.GUID)
                {
                    return false;
                }
                fileSystemServiceAgent = new FileSystemServiceAgent(OntoMsg_Module.ModuleDataExchanger.CurrentWebApp.EntryUrl, OntoMsg_Module.ModuleDataExchanger.CurrentWebApp.EntrySessionPath, oItemUser, oItemGroup);
            }
            

            return true;
            
        }

       
        private bool CheckAuthentication(string sessionId)
        {
            var textUserName = "";
            var textGroup = "";
            var textPassword = "";
            var docs = serviceAgentElastic.UserDbSelector.GetData_Documents(query: WebSocketBase.QueryParam_Session + ":" + sessionId, strIndex: "sessiondata", strType: "session");

            webSocketServiceAgent.RegisterEndpoint(new OntoMsg_Module.Notifications.ChannelEndpoint
            {
                ChannelTypeId = Channels.Login,
                EndpointType = OntoMsg_Module.Notifications.EndpointType.Receiver
            });

            webSocketServiceAgent.RegisterEndpoint(new OntoMsg_Module.Notifications.ChannelEndpoint
            {
                ChannelTypeId = Notifications.NotifyChanges.Channel_DecodePasswords,
                EndpointType = OntoMsg_Module.Notifications.EndpointType.Receiver
            });

            webSocketServiceAgent.RegisterEndpoint(new OntoMsg_Module.Notifications.ChannelEndpoint
            {
                ChannelTypeId = Channels.ParameterList,
                EndpointType = OntoMsg_Module.Notifications.EndpointType.Receiver
            });

            if (docs.Any())
            {

                var doc = docs.First();
                if (doc.Dict.ContainsKey(Notifications.NotifyChanges.ViewModel_DateTimeStamp_Expire))
                {
                    var expireObject = doc.Dict[Notifications.NotifyChanges.ViewModel_DateTimeStamp_Expire];
                    var expireDate = expireObject as DateTime?;
                    if (expireDate != null && expireDate > DateTime.Now)
                    {
                        if (doc.Dict.ContainsKey(Notifications.NotifyChanges.View_Text_UserName))
                        {
                            textUserName = doc.Dict[Notifications.NotifyChanges.View_Text_UserName].ToString();
                        }
                        if (doc.Dict.ContainsKey(Notifications.NotifyChanges.View_Text_Group))
                        {
                            textGroup = doc.Dict[Notifications.NotifyChanges.View_Text_Group].ToString();
                        }
                        if (doc.Dict.ContainsKey(Notifications.NotifyChanges.View_Text_Password))
                        {
                            textPassword = doc.Dict[Notifications.NotifyChanges.View_Text_Password].ToString();
                        }
                        if (doc.Dict.ContainsKey(Notifications.NotifyChanges.View_IsUserSender))
                        {
                            isUserSender = (bool)doc.Dict[Notifications.NotifyChanges.View_IsUserSender];
                        }
                        var isValid = IsValid(textUserName, textGroup, textPassword);
                        if (isValid)
                        {
                            
                            
                            return true;
                        }
                    }
                }


            }

            return false;
        }

       
        private void Initialize()
        {
            var stateMachine = new ControllerStateMachine(StateMachineType.BlockSelectingSelected | StateMachineType.BlockLoadingSelected);
            stateMachine.IsControllerListen = true;
            StateMachine = stateMachine;
            stateMachine.PropertyChanged += StateMachine_PropertyChanged;
            stateMachine.loadSelectedItem += StateMachine_loadSelectedItem;
            stateMachine.loginSucceded += StateMachine_loginSucceded;
            stateMachine.openedSocket += StateMachine_openedSocket;
            stateMachine.closedSocket += StateMachine_closedSocket;

            securityController = new SecurityController(localConfig.Globals);
            serviceAgentElastic = new ServiceAgent_Elastic(localConfig);
            viewMetaFactory = new ViewMetaFactory(localConfig);

        }

        private void StateMachine_closedSocket()
        {
            var result = fileSystemServiceAgent?.RemoveAllResources();
            serviceAgentElastic = null;
            viewMetaFactory = null;
            securityController = null;
        }

        private void StateMachine_openedSocket()
        {
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Sender,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            if (CheckAuthentication(webSocketServiceAgent.DataText_SessionId))
            {
                Url_Navigation = ModuleDataExchanger.CurrentWebApp.EntryUrl + "/Navigation/Navigation.html?Session=" + webSocketServiceAgent.DataText_SessionId;
            }


            webSocketServiceAgent.Context.WebSocket.Log.Debug("Sending Model");
            webSocketServiceAgent.SendModel();
        }

        private void StateMachine_loginSucceded()
        {
            throw new NotImplementedException();
        }

        private void StateMachine_loadSelectedItem(clsOntologyItem oItemSelected)
        {
            
        }

        private void StateMachine_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            
        }

        public void InitializeViewController(WebsocketServiceAgent webSocketServiceAgent)
        {
            this.webSocketServiceAgent = webSocketServiceAgent;
            this.webSocketServiceAgent.PropertyChanged += WebSocketServiceAgent_PropertyChanged;
            this.webSocketServiceAgent.comServerOnMessage += WebSocketServiceAgent_comServerOnMessage;

            this.webSocketServiceAgent.IsLoginController = true;

            if (string.IsNullOrEmpty(webSocketServiceAgent.IdEntryView) || !localConfig.Globals.is_GUID(webSocketServiceAgent.IdEntryView))
            {
                this.webSocketServiceAgent.CloseSocket("No Entry-View");
                return;
            }

            var entryViewData = serviceAgentElastic.GetEntryViewData(webSocketServiceAgent.IdEntryView);
            if (entryViewData == null)
            {
                this.webSocketServiceAgent.CloseSocket("No Entry-View Data found");
                return;
            }

            var entryViewItem = viewMetaFactory.GetWebAppItem(entryViewData);
            if (entryViewItem == null)
            {
                this.webSocketServiceAgent.CloseSocket("No Entry-View Item can be created");
                return;
            }

            ModuleDataExchanger.CurrentWebApp = entryViewItem;

            
        }

      
        private void WebSocketServiceAgent_comServerOnMessage(OntoMsg_Module.Notifications.InterServiceMessage message)
        {
            if (message.ChannelId == Channels.Login)
            {
                var answer = new InterServiceMessage
                {
                    ChannelId = message.ChannelId,
                    ReceiverId = message.SenderId,
                    SenderId = webSocketServiceAgent.EndpointId,
                    CommunicationStatus = CommunicationStatus.None,
                    OItems = new List<OntologyClasses.BaseClasses.clsOntologyItem>()
                };

                
                if (securityController.IsInitialized)
                {
                    oItemUser.Mark = isUserSender;
                    
                    answer.OItems.Add(oItemUser);
                    answer.OItems.Add(oItemGroup);
                }
                else
                {
                    answer.OItems.Add(localConfig.Globals.LState_Error.Clone());
                }

                webSocketServiceAgent.SendInterModMessage(answer);
            }
            else if (message.ChannelId == Notifications.NotifyChanges.Channel_DecodePasswords)
            {
                message.OItems.ForEach(oItem =>
                {
                    oItem.Additional1 = securityController.DecodePassword(oItem);
                });

                var answer = new InterServiceMessage
                {
                    ChannelId = message.ChannelId,
                    ReceiverId = message.SenderId,
                    SenderId = webSocketServiceAgent.EndpointId,
                    CommunicationStatus = CommunicationStatus.None,
                    OItems = message.OItems.Select(oItem => oItem).ToList()
                };

                webSocketServiceAgent.SendInterModMessage(answer);
            }
            else if (message.ChannelId == Channels.ParameterList)
            {
                var answer = new InterServiceMessage
                {
                    ChannelId = message.ChannelId,
                    ReceiverId = message.SenderId,
                    SenderId = webSocketServiceAgent.EndpointId,
                    CommunicationStatus = CommunicationStatus.None,
                    GenericParameterItems = webSocketServiceAgent.ViewArguments.ToList<object>()
                };

                webSocketServiceAgent.SendInterModMessage(answer);
            }
        }

        public bool ControllerFunctionIsChecked()
        {
            return securityController != null ? securityController.IsInitialized : false;
        }

        public List<ViewModelProperty> GetViewModelProperties(bool onlySend = true, ViewItemType viewItemType = ViewItemType.All, ViewItemClass viewItemClass = ViewItemClass.All)
        {
            return ViewModelProperties.Where(viewItemProp => (onlySend ? viewItemProp.ViewModelAttribute.Send : 1==1) 
                && (viewItemType != ViewItemType.All ? viewItemProp.ViewModelAttribute.ViewItemType == viewItemType : 1==1)
                && (viewItemClass != ViewItemClass.All ? viewItemProp.ViewModelAttribute.ViewItemClass == viewItemClass : 1 == 1)).ToList();
        }
    }
}
