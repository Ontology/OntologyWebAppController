﻿using OntologyWebAppController.Notifications;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyWebAppController.Controllers
{
    public class MessageViewModel : ViewModelBase
    {
        private bool issuccessful_Login;

        public bool IsSuccessful_Login
        {
            get { return issuccessful_Login; }
            set
            {
                if (issuccessful_Login == value) return;

                issuccessful_Login = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsSuccessful_Login);

            }
        }

        private bool isenabled_Ok;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "okButton", ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_Ok
        {
            get { return isenabled_Ok; }
            set
            {
                if (isenabled_Ok == value) return;

                isenabled_Ok = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_Ok);

            }
        }

        private bool isvisible_Ok;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "okButton", ViewItemType = ViewItemType.Visible)]
        public bool IsVisible_Ok
        {
            get { return isvisible_Ok; }
            set
            {
                if (isvisible_Ok == value) return;

                isvisible_Ok = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsVisible_Ok);

            }
        }

        private string label_Ok;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "okButton", ViewItemType = ViewItemType.Content)]
        public string Label_Ok
        {
            get { return label_Ok; }
            set
            {
                if (label_Ok == value) return;

                label_Ok = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_Ok);

            }
        }

        private bool isenabled_Cancel;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "cancelButton", ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_Cancel
        {
            get { return isenabled_Cancel; }
            set
            {
                if (isenabled_Cancel == value) return;

                isenabled_Cancel = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_Cancel);

            }
        }

        private bool isvisible_Cancel;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "cancelButton", ViewItemType = ViewItemType.Visible)]
        public bool IsVisible_Cancel
        {
            get { return isvisible_Cancel; }
            set
            {
                if (isvisible_Cancel == value) return;

                isvisible_Cancel = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsVisible_Cancel);

            }
        }

        private string label_Cancel;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "cancelButton", ViewItemType = ViewItemType.Content)]
        public string Label_Cancel
        {
            get { return label_Cancel; }
            set
            {
                if (label_Cancel == value) return;

                label_Cancel = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_Cancel);

            }
        }

        private bool isenabled_Yes;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "yesButton", ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_Yes
        {
            get { return isenabled_Yes; }
            set
            {
                if (isenabled_Yes == value) return;

                isenabled_Yes = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_Yes);

            }
        }

        private bool isvisible_Yes;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "yesButton", ViewItemType = ViewItemType.Visible)]
        public bool IsVisible_Yes
        {
            get { return isvisible_Yes; }
            set
            {
                if (isvisible_Yes == value) return;

                isvisible_Yes = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsVisible_Yes);

            }
        }

        private string label_Yes;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "yesButton", ViewItemType = ViewItemType.Content)]
        public string Label_Yes
        {
            get { return label_Yes; }
            set
            {
                if (label_Yes == value) return;

                label_Yes = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_Yes);

            }
        }

        private bool isenabled_No;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "noButton", ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_No
        {
            get { return isenabled_No; }
            set
            {
                if (isenabled_No == value) return;

                isenabled_No = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_No);

            }
        }

        private bool isvisible_No;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "noButton", ViewItemType = ViewItemType.Visible)]
        public bool IsVisible_No
        {
            get { return isvisible_No; }
            set
            {
                if (isvisible_No == value) return;

                isvisible_No = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsVisible_No);

            }
        }

        private string label_No;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "noButton", ViewItemType = ViewItemType.Content)]
        public string Label_No
        {
            get { return label_No; }
            set
            {
                if (label_No == value) return;

                label_No = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_No);

            }
        }

        private string text_MessageArea;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Container, ViewItemId = "messageArea", ViewItemType = ViewItemType.Content)]
        public string Text_MessageArea
        {
            get { return text_MessageArea; }
            set
            {
                if (text_MessageArea == value) return;

                text_MessageArea = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Text_MessageArea);

            }
        }

        private bool isvisible_MessageArea;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Container, ViewItemId = "messageArea", ViewItemType = ViewItemType.Visible)]
        public bool IsVisible_MessageArea
        {
            get { return isvisible_MessageArea; }
            set
            {
                if (isvisible_MessageArea == value) return;

                isvisible_MessageArea = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsVisible_MessageArea);

            }
        }

        private bool isenabled_MessageArea;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Container, ViewItemId = "messageArea", ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_MessageArea
        {
            get { return isenabled_MessageArea; }
            set
            {
                if (isenabled_MessageArea == value) return;

                isenabled_MessageArea = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_MessageArea);

            }
        }
    }
}
