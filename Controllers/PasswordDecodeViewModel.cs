﻿using OntologyWebAppController.Notifications;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using SecurityModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyWebAppController.Controllers
{
    public class PasswordDecodeViewModel : ViewModelBase
    {
        private bool issuccessful_Login;

        public bool IsSuccessful_Login
        {
            get { return issuccessful_Login; }
            set
            {
                if (issuccessful_Login == value) return;

                issuccessful_Login = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsSuccessful_Login);

            }
        }

        private JqxColumnList columnconfig_Grid;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Grid, ViewItemId = "grid", ViewItemType = ViewItemType.ColumnConfig)]
        public JqxColumnList ColumnConfig_Grid
        {
            get { return columnconfig_Grid; }
            set
            {
                if (columnconfig_Grid == value) return;

                columnconfig_Grid = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_ColumnConfig_Grid);

            }
        }

        private JqxDataSource jqxdatasource_Grid;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Grid, ViewItemId = "grid", ViewItemType = ViewItemType.DataSource)]
        public JqxDataSource JqxDataSource_Grid
        {
            get { return jqxdatasource_Grid; }
            set
            {
                if (jqxdatasource_Grid == value) return;

                jqxdatasource_Grid = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_JqxDataSource_Grid);

            }
        }

        private bool isvisible_ContextMenu;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.ContextMenu, ViewItemId = "contextMenu", ViewItemType = ViewItemType.Visible)]
        public bool IsVisible_ContextMenu
        {
            get { return isvisible_ContextMenu; }
            set
            {
                if (isvisible_ContextMenu == value) return;

                isvisible_ContextMenu = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsVisible_ContextMenu);

            }
        }

        private JqxDataSource jqxDataSource_ContextMenu;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.ContextMenu, ViewItemId = "contextMenu", ViewItemType = ViewItemType.Content)]
        public JqxDataSource JqxDataSource_ContextMenu
        {
            get { return jqxDataSource_ContextMenu; }
            set
            {
                if (jqxDataSource_ContextMenu == value) return;

                jqxDataSource_ContextMenu = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_JqxDataSource_ContextMenu);

            }
        }

        private bool isenabled_ContextMenu;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.ContextMenu, ViewItemId = "contextMenu", ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_ContextMenu
        {
            get { return isenabled_ContextMenu; }
            set
            {
                if (isenabled_ContextMenu == value) return;

                isenabled_ContextMenu = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_ContextMenu);

            }
        }

        private MenuEntry contextmenuentry_ContextMenuEntryChange;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.ContextMenuEntry, ViewItemId = "contextMenu", ViewItemType = ViewItemType.Change)]
        public MenuEntry ContextMenuEntry_ContextMenuEntryChange
        {
            get { return contextmenuentry_ContextMenuEntryChange; }
            set
            {

                contextmenuentry_ContextMenuEntryChange = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_ContextMenuEntry_ContextMenuEntryChange);

            }
        }

        private PasswordItem passworditem_ToEdit;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "passwordItemToEdit", ViewItemType = ViewItemType.Other)]
        public PasswordItem PasswordItem_ToEdit
        {
            get { return passworditem_ToEdit; }
            set
            {
                if (passworditem_ToEdit == value) return;

                passworditem_ToEdit = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_PasswordItem_ToEdit);

            }
        }

        private bool isenabled_CopyPassword;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "cpyPassword", ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_CopyPassword
        {
            get { return isenabled_CopyPassword; }
            set
            {
                if (isenabled_CopyPassword == value) return;

                isenabled_CopyPassword = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_CopyPassword);

            }
        }

        private bool isenabled_RemoveUser;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "removeUser", ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_RemoveUser
        {
            get { return isenabled_RemoveUser; }
            set
            {
                if (isenabled_RemoveUser == value) return;

                isenabled_RemoveUser = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_RemoveUser);

            }
        }

        private bool istoggled_ListenUser;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.ToggleButton, ViewItemId = "listenUser", ViewItemType = ViewItemType.Checked)]
        public bool IsToggled_ListenUser
        {
            get { return istoggled_ListenUser; }
            set
            {
                if (istoggled_ListenUser == value) return;

                istoggled_ListenUser = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsToggled_ListenUser);

            }
        }

        private bool isenabled_ListenUser;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.ToggleButton, ViewItemId = "listenUser", ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_ListenUser
        {
            get { return isenabled_ListenUser; }
            set
            {
                if (isenabled_ListenUser == value) return;

                isenabled_ListenUser = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_ListenUser);

            }
        }

        private string text_User;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpUser", ViewItemType = ViewItemType.Content)]
        public string Text_User
        {
            get { return text_User; }
            set
            {
                if (text_User == value) return;

                text_User = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Text_User);

            }
        }

        private string text_PasswordRepeat;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpPasswordRepeat", ViewItemType = ViewItemType.Content)]
        public string Text_PasswordRepeat
        {
            get { return text_PasswordRepeat; }
            set
            {
                if (text_PasswordRepeat == value) return;

                text_PasswordRepeat = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Text_PasswordRepeat);

            }
        }

        private bool isvisible_PasswordRepeat;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpPasswordRepeat", ViewItemType = ViewItemType.Visible)]
        public bool IsVisible_PasswordRepeat
        {
            get { return isvisible_PasswordRepeat; }
            set
            {
                if (isvisible_PasswordRepeat == value) return;

                isvisible_PasswordRepeat = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsVisible_PasswordRepeat);

            }
        }

        private string datatext_ClassId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "classId", ViewItemType = ViewItemType.Other)]
        public string DataText_ClassId
        {
            get { return datatext_ClassId; }
            set
            {
                if (datatext_ClassId == value) return;

                datatext_ClassId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_ClassId);

            }
        }

        private string datatext_ObjectId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "objectId", ViewItemType = ViewItemType.Other)]
        public string DataText_ObjectId
        {
            get { return datatext_ObjectId; }
            set
            {
                if (datatext_ObjectId == value) return;

                datatext_ObjectId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_ObjectId);

            }
        }

        private string datatext_RelationTypeId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "relationTypeId", ViewItemType = ViewItemType.Other)]
        public string DataText_RelationTypeId
        {
            get { return datatext_RelationTypeId; }
            set
            {
                if (datatext_RelationTypeId == value) return;

                datatext_RelationTypeId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_RelationTypeId);

            }
        }

        private string datatext_AttributeTypeId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "attributeTypeId", ViewItemType = ViewItemType.Other)]
        public string DataText_AttributeTypeId
        {
            get { return datatext_AttributeTypeId; }
            set
            {
                if (datatext_AttributeTypeId == value) return;

                datatext_AttributeTypeId = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_AttributeTypeId);

            }
        }

        private bool ischecked_ShowPassword;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "showPassword", ViewItemType = ViewItemType.Checked)]
        public bool IsChecked_ShowPassword
        {
            get { return ischecked_ShowPassword; }
            set
            {
                if (ischecked_ShowPassword == value) return;

                ischecked_ShowPassword = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsChecked_ShowPassword);

            }
        }

        private string text_ShowPasswordIcon;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "showPassword", ViewItemType = ViewItemType.IconLabel)]
        public string Text_ShowPasswordIcon
        {
            get { return text_ShowPasswordIcon; }
            set
            {
                if (text_ShowPasswordIcon == value) return;

                text_ShowPasswordIcon = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Text_ShowPasswordIcon);

            }
        }

        private bool isenabled_ShowPassword;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "showPassword", ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_ShowPassword
        {
            get { return isenabled_ShowPassword; }
            set
            {
                if (isenabled_ShowPassword == value) return;

                isenabled_ShowPassword = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_ShowPassword);

            }
        }

    }
}
