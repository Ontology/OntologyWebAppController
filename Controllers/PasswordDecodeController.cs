﻿using Newtonsoft.Json;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntologyWebAppController.Translations;
using OntoMsg_Module;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using OntoMsg_Module.Notifications;
using OntoMsg_Module.WebSocketServices;
using SecurityModule.Models;
using SecurityModule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using OntologyWebAppController.Services;
using OntoWebCore.Models;
using OntoWebCore.Factories;
using OntoMsg_Module.StateMachines;
using System.ComponentModel;

namespace OntologyWebAppController.Controllers
{
    public class PasswordDecodeController : PasswordDecodeViewModel, IViewController
    {
        private WebsocketServiceAgent webSocketServiceAgent;

        private TranslationController translationController = new TranslationController();

        private clsLocalConfig localConfig;


        private clsOntologyItem oItem;

        private MenuEntry newPasswordMenuEntry;
        private MenuEntry copyPasswordMenuEntry;
        private MenuEntry showPasswordMenuEntry;
    
        private string fileNameContextMenuJson;
        private string fileNamePasswordGridJson;

        private SecurityController securityController;
        private List<PasswordItem> securityItems;

        private PasswordItem selectedPasswordItem;
        private MenuEntry selectedMenuEntry;

        private ServiceAgent_Elastic serviceAgentElastic;

        private PasswordItem lastDecodedPasswordItem;

        private string iconLabelLocked = "<i class=\"fa fa-lock\" aria-hidden=\"true\" ></i>";
        private string iconLabelUnLocked = "<i class=\"fa fa-unlock\" aria-hidden=\"true\" ></i>";

        public IControllerStateMachine StateMachine { get; private set; }
        public ControllerStateMachine LocalStateMachine
        {
            get
            {
                if (StateMachine == null)
                {
                    return null;
                }

                return (ControllerStateMachine)StateMachine;
            }
        }

        public PasswordDecodeController()
        {
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            Initialize();
            PropertyChanged += PasswordDecoderController_PropertyChanged;
        }

        private void PasswordDecoderController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var property = ViewModelProperties.FirstOrDefault(viewProperty => viewProperty.Property.Name == e.PropertyName);

            if (property == null) return;

            property.ViewItem.AddValue(property.Property.GetValue(this));

            webSocketServiceAgent.SendPropertyChange(e.PropertyName);

            if (e.PropertyName == Notifications.NotifyChanges.ViewModel_IsChecked_ShowPassword)
            {
                SetPasswordItem();
            }

        }

        private void Initialize()
        {
            var stateMachine = new ControllerStateMachine(StateMachineType.BlockSelectingSelected | StateMachineType.BlockLoadingSelected);
            stateMachine.IsControllerListen = true;
            StateMachine = stateMachine;
            stateMachine.PropertyChanged += StateMachine_PropertyChanged;
            stateMachine.loadSelectedItem += StateMachine_loadSelectedItem;
            stateMachine.loginSucceded += StateMachine_loginSucceded;
            stateMachine.openedSocket += StateMachine_openedSocket;
            stateMachine.closedSocket += StateMachine_closedSocket;

            securityController = new SecurityController(localConfig.Globals);
            securityController.loadedSecurityItems += SecurityController_loadedSecurityItems;

            serviceAgentElastic = new ServiceAgent_Elastic(localConfig);

            PasswordItem_ToEdit = new PasswordItem();

            IsEnabled_ContextMenu = true;
            IsEnabled_CopyPassword = true;
            IsEnabled_ListenUser = true;
            IsEnabled_RemoveUser = true;
            IsVisible_PasswordRepeat = true;

           

        }

        private void StateMachine_closedSocket()
        {
            webSocketServiceAgent.RemoveAllResources();
        }

        private void StateMachine_openedSocket()
        {
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Sender,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });



            //if (webSocketServiceAgent.Request.ContainsKey("ListType"))
            //{

            //}
        }

        private void StateMachine_loginSucceded()
        {
            
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.ParameterList,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });


            fileNameContextMenuJson = Guid.NewGuid().ToString() + ".json";
            var sessionFile = webSocketServiceAgent.RequestWriteStream(fileNameContextMenuJson);



            newPasswordMenuEntry = new MenuEntry
            {
                Id = Guid.NewGuid().ToString(),
                Name = translationController.MenuEntry_New,
                IsVisible = true

            };

            copyPasswordMenuEntry = new MenuEntry
            {
                Id = Guid.NewGuid().ToString(),
                Name = translationController.MenuEntry_Copy,
                IsVisible = true
            };

            showPasswordMenuEntry = new MenuEntry
            {
                Id = Guid.NewGuid().ToString(),
                Name = translationController.MenuEntry_Show,
                IsVisible = true
            };

            var jqxMenuEntries = new List<MenuEntry>
            {
                newPasswordMenuEntry,
                copyPasswordMenuEntry,
                showPasswordMenuEntry
            };

            using (sessionFile.StreamWriter)
            {

                using (var jsonWriter = new JsonTextWriter(sessionFile.StreamWriter))
                {
                    var jsonString = JsonConvert.SerializeObject(jqxMenuEntries);

                    jsonWriter.WriteRaw(jsonString);
                }
            }

            var jqxDataSource = new JqxDataSource(JsonDataType.Json)
            {
                datafields = new List<DataField>
                {
                    new DataField(DataFieldType.String)
                    {
                        name = "Id"
                    },
                    new DataField(DataFieldType.String)
                    {
                        name = "Name"
                    },
                    new DataField(DataFieldType.String)
                    {
                        name = "ParentId"
                    }
                },
                url = sessionFile.FileUri.AbsoluteUri,
                id = "Id"
            };

            JqxDataSource_ContextMenu = jqxDataSource;
            
            


            fileNamePasswordGridJson = Guid.NewGuid().ToString() + ".json";

            var sessionFileGrid = webSocketServiceAgent.RequestWriteStream(fileNamePasswordGridJson);

            var passwordList = new List<Password>();

            using (sessionFileGrid.StreamWriter)
            {
                using (var jsonTextWriter = new Newtonsoft.Json.JsonTextWriter(sessionFileGrid.StreamWriter))
                {
                    jsonTextWriter.WriteRaw(Newtonsoft.Json.JsonConvert.SerializeObject(passwordList));
                }
            }

            ColumnConfig_Grid = GridFactory.CreateColumnList(typeof(PasswordItem));
            JqxDataSource_Grid = GridFactory.CreateJqxDataSource(typeof(PasswordItem), 0, 20, sessionFileGrid.FileUri.AbsoluteUri);

            ContextMenuEntry_ContextMenuEntryChange = newPasswordMenuEntry;
            ContextMenuEntry_ContextMenuEntryChange = copyPasswordMenuEntry;
            ContextMenuEntry_ContextMenuEntryChange = showPasswordMenuEntry;

            IsEnabled_ContextMenu = false;
            IsEnabled_CopyPassword = false;
            IsEnabled_ListenUser = false;
            IsEnabled_RemoveUser = false;
            IsVisible_PasswordRepeat = false;



           
            if (!string.IsNullOrEmpty(DataText_ObjectId))
            {
                var objectId = DataText_ObjectId;

                oItem = serviceAgentElastic.GetOItem(objectId, localConfig.Globals.Type_Object);

                ReferenceReceived(oItem);
            }


           
        }

        private void StateMachine_loadSelectedItem(clsOntologyItem oItemSelected)
        {
            
        }

        private void StateMachine_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(OntoMsg_Module.StateMachines.ControllerStateMachine.LoginSuccessful))
            {
                IsSuccessful_Login = LocalStateMachine.LoginSuccessful;
            }
        }

        private void SecurityController_loadedSecurityItems(List<PasswordItem> passwordItems)
        {
            securityItems = passwordItems;
            if (passwordItems != null)
            {

                fileNamePasswordGridJson = Guid.NewGuid().ToString() + ".json";

                var sessionFileGrid = webSocketServiceAgent.RequestWriteStream(fileNamePasswordGridJson);

                var passwordList = passwordItems;

                using (sessionFileGrid.StreamWriter)
                {
                    using (var jsonTextWriter = new Newtonsoft.Json.JsonTextWriter(sessionFileGrid.StreamWriter))
                    {
                        jsonTextWriter.WriteRaw(Newtonsoft.Json.JsonConvert.SerializeObject(passwordList));
                    }
                }

                JqxDataSource_Grid = GridFactory.CreateJqxDataSource(typeof(PasswordItem), 0, 20, sessionFileGrid.FileUri.AbsoluteUri);
            }
        }

        public void InitializeViewController(WebsocketServiceAgent webSocketServiceAgent)
        {
            this.webSocketServiceAgent = webSocketServiceAgent;
            this.webSocketServiceAgent.PropertyChanged += WebSocketServiceAgent_PropertyChanged;
            this.webSocketServiceAgent.comServerOnMessage += WebSocketServiceAgent_comServerOnMessage;
            this.webSocketServiceAgent.comServerOpened += WebSocketServiceAgent_comServerOpened;




        }

      

        private void WebSocketServiceAgent_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_DataText_SessionId)
            {
                

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Command_RequestedCommand)
            {
                

                if (webSocketServiceAgent.Command_RequestedCommand == "SelectedObjectRows")
                {
                    selectedPasswordItem = null;
                    copyPasswordMenuEntry.IsEnabled = false;
                    showPasswordMenuEntry.IsEnabled = false;

                    var instanceIds = webSocketServiceAgent.Request["IdItems"];
                    if (instanceIds == null) return;

                    var selectedNodeIds = instanceIds.ToString().Split(',').ToList();

                    if (selectedNodeIds.Count == 1)
                    {
                        var selectedNodeId = selectedNodeIds[0];
                        selectedPasswordItem = securityItems.FirstOrDefault(secItem => secItem.Id == selectedNodeId);

                        if (selectedPasswordItem != null)
                        {
                            copyPasswordMenuEntry.IsEnabled = true;
                            showPasswordMenuEntry.IsEnabled = true;
                            
                        }
                        
                    }
                    ContextMenuEntry_ContextMenuEntryChange = copyPasswordMenuEntry;
                    ContextMenuEntry_ContextMenuEntryChange = showPasswordMenuEntry;

                }
                else if (webSocketServiceAgent.Command_RequestedCommand == "ClickedMenuItem")
                {
                    selectedMenuEntry = null;
                    var selectedNodeId = webSocketServiceAgent.Request["Id"].ToString();
                    

                    
                    if (selectedNodeId == newPasswordMenuEntry.Id)
                    {
                        selectedMenuEntry = newPasswordMenuEntry;
                    }
                    else if (selectedNodeId == copyPasswordMenuEntry.Id)
                    {
                        if (selectedPasswordItem == null) return;
                        selectedMenuEntry = copyPasswordMenuEntry;

                        var passwordItem = serviceAgentElastic.GetOItem(selectedPasswordItem.Id, localConfig.Globals.Type_Object);

                        if (passwordItem == null) return;

                        var interComMessage = new InterServiceMessage
                        {
                            ChannelId = Notifications.NotifyChanges.Channel_DecodePasswords,
                            OItems = new List<clsOntologyItem> { passwordItem }
                        };

                        webSocketServiceAgent.SendInterModMessage(interComMessage);
                    }
                    else if (selectedNodeId == showPasswordMenuEntry.Id)
                    {
                        if (selectedPasswordItem == null) return;
                        selectedMenuEntry = showPasswordMenuEntry;

                        var passwordItem = serviceAgentElastic.GetOItem(selectedPasswordItem.Id, localConfig.Globals.Type_Object);

                        if (passwordItem == null) return;

                        var interComMessage = new InterServiceMessage
                        {
                            ChannelId = Notifications.NotifyChanges.Channel_DecodePasswords,
                            OItems = new List<clsOntologyItem> { passwordItem}
                        };

                        webSocketServiceAgent.SendInterModMessage(interComMessage);

                        
                    }
                    
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == "ClickedShowPassword")
                {
                    IsChecked_ShowPassword = !IsChecked_ShowPassword;
                }


            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Event_RequestEvent)
            {

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_ChangedProperty)
            {

            }


        }

        private void WebSocketServiceAgent_comServerOpened()
        {
            var authenticationRequest = new InterServiceMessage
            {
                ChannelId = Channels.Login,
                SenderId = webSocketServiceAgent.EndpointId
            };

            webSocketServiceAgent.SendInterModMessage(authenticationRequest);

        }

        private void ReferenceReceived(clsOntologyItem oItemReceived)
        {
            newPasswordMenuEntry.IsEnabled = false;
            showPasswordMenuEntry.IsEnabled = false;
            copyPasswordMenuEntry.IsEnabled = false;

            if (oItemReceived != null)
            {
                newPasswordMenuEntry.IsEnabled = true;
                securityController.GetSecurityItems(oItem);
            }

            ContextMenuEntry_ContextMenuEntryChange = newPasswordMenuEntry;
            ContextMenuEntry_ContextMenuEntryChange = showPasswordMenuEntry;
            ContextMenuEntry_ContextMenuEntryChange = copyPasswordMenuEntry;
        }

        private void WebSocketServiceAgent_comServerOnMessage(OntoMsg_Module.Notifications.InterServiceMessage message)
        {
            

            if (message.ReceiverId != null && message.ReceiverId != webSocketServiceAgent.EndpointId) return;
            if (message.ChannelId == Channels.ParameterList)
            {
                newPasswordMenuEntry.IsEnabled = false;
                showPasswordMenuEntry.IsEnabled = false;
                copyPasswordMenuEntry.IsEnabled = false;

                var oItemMessage = message.OItems.LastOrDefault();
                if (oItemMessage == null) return;
                if ((oItem != null && oItemMessage.GUID != oItem.GUID) || oItem == null)
                {
                    oItem = oItemMessage;

                    ReferenceReceived(oItem);
 
                }

                

            }
            else if (message.ChannelId == Notifications.NotifyChanges.Channel_DecodePasswords)
            {
                var passwordItem = message.OItems.FirstOrDefault();

                if (passwordItem == null) return;

                if (selectedMenuEntry == showPasswordMenuEntry)
                {
                    lastDecodedPasswordItem = new PasswordItem
                    {
                        Id = selectedPasswordItem.Id,
                        IdRef = selectedPasswordItem.IdRef,
                        IdUser = selectedPasswordItem.IdUser,
                        NameRef = selectedPasswordItem.NameRef,
                        NameUser = selectedPasswordItem.NameUser,
                        ParentRef = selectedPasswordItem.ParentRef,
                        PasswordMask = passwordItem.Additional1
                    };

                    SetPasswordItem();

                }
                
                
            }


        }


        private void SetPasswordItem()
        {
            
            var passwordItemToEdit = new PasswordItem
            {
                Id = selectedPasswordItem.Id,
                IdRef = selectedPasswordItem.IdRef,
                IdUser = selectedPasswordItem.IdUser,
                NameRef = selectedPasswordItem.NameRef,
                NameUser = selectedPasswordItem.NameUser,
                ParentRef = selectedPasswordItem.ParentRef,
                PasswordMask = selectedPasswordItem.PasswordMask
            };
            

            
            if (IsChecked_ShowPassword)
            {
                IsEnabled_CopyPassword = true;
                Text_ShowPasswordIcon = iconLabelUnLocked;
                passwordItemToEdit.PasswordMask = lastDecodedPasswordItem.PasswordMask;
            }
            else
            {
                IsEnabled_CopyPassword = false;
                Text_ShowPasswordIcon = iconLabelLocked;
            }

            PasswordItem_ToEdit = passwordItemToEdit;
        }

        public List<ViewModelProperty> GetViewModelProperties(bool onlySend = true, ViewItemType viewItemType = ViewItemType.All, ViewItemClass viewItemClass = ViewItemClass.All)
        {
            return ViewModelProperties.Where(viewItemProp => (onlySend ? viewItemProp.ViewModelAttribute.Send : 1 == 1)
                && (viewItemType != ViewItemType.All ? viewItemProp.ViewModelAttribute.ViewItemType == viewItemType : 1 == 1)
                && (viewItemClass != ViewItemClass.All ? viewItemProp.ViewModelAttribute.ViewItemClass == viewItemClass : 1 == 1)).ToList();
        }
    }
}
