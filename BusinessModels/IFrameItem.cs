﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyWebAppController.BusinessModels
{
    public enum IFrameItemType
    {
        Panel = 0,
        NewWindow = 1,
        TabPage = 2
        
    }
    public class IFrameItem
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string src { get; set; }
        public IFrameItemType ItemType { get; set; }
        public string ContainerId { get; set; }

    }
}
