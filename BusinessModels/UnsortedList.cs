﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyWebAppController.BusinessModels
{
    public class UnsortedList
    {
        public string Id { get; set; }
        private List<ListItem> listItems = new List<ListItem>();
    
        public List<ListItem> ListItems
        {
            get { return listItems; }
            set
            {
                listItems = value;
            }
        }

    }
}
