﻿using Newtonsoft.Json;
using OntologyWebAppController.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace OntologyWebAppController.BusinessModels
{
    public class ViewTreeNode
    {
        [JQMenu(DataAttribute ="id")]
        public string Id { get; set; }

        [JQMenu(DataAttribute = "text")]
        public string Name { get; set; }

        public List<ViewTreeNode> SubNodes { get; set; }
        public ViewTreeNode ParentNode { get; set; }

        public string IdNodeImage { get; set; }

        //[DhtmlXTree(DataAttribute = "im2", OrderId = 5)]
        public string NameNodeImage { get; set; }

        //[DhtmlXTree(DataAttribute = "im1", OrderId = 4)]
        public string NameNodeImageSelected
        {
            get { return NameNodeImage; }
        }

        public string NameNodeImageNoChildren
        {
            get { return NameNodeImage != null ?  NameNodeImage + ".png" : null; }
        }

        public string IdPath { get; set; }

        
        public bool WriteXMLDoc(XmlWriter xmlWriter)
        {
            try
            {
                xmlWriter.WriteStartElement("item");
                this.GetType().GetProperties().Cast<PropertyInfo>().ToList().ForEach(propItem =>
                {
                    var attribute = propItem.GetCustomAttribute<DhtmlXTreeAttribute>();
                    var value = propItem.GetValue(this);
                    if (value == null) return;
                    if (attribute != null)
                    {
                        xmlWriter.WriteAttributeString(attribute.DataAttribute, System.Web.HttpUtility.HtmlEncode(value.ToString()));
                        
                    }


                });

                if (SubNodes != null && SubNodes.Any())
                {
                    SubNodes.ForEach(subNode =>
                    {
                        subNode.WriteXMLDoc(xmlWriter);
                    });

                }

                xmlWriter.WriteEndElement();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public XmlNode GetXMLForDHTMLxControl(XmlDocument xmlDocument)
        {

            
            var xmlNode = xmlDocument.CreateNode(XmlNodeType.Element, "item", "");

            this.GetType().GetProperties().Cast<PropertyInfo>().ToList().ForEach(propItem =>
            {


                var attribute = propItem.GetCustomAttribute<DhtmlXTreeAttribute>();

                if (attribute != null)
                {

                    var xmlAttributeNode = (XmlAttribute)xmlDocument.CreateNode(XmlNodeType.Attribute, attribute.DataAttribute, "");
                    xmlAttributeNode.Value = System.Web.HttpUtility.HtmlEncode(propItem.GetValue(this).ToString());
                    xmlNode.Attributes.Append(xmlAttributeNode);
                }


            });

            

            if (SubNodes != null && SubNodes.Any())
            {
                SubNodes.ForEach(subNode =>
                {
                    xmlNode.AppendChild(subNode.GetXMLForDHTMLxControl(xmlDocument));
                });
                
            }
            
            return xmlNode;
        }

    }
}
