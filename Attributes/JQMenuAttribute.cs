﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyWebAppController.Attributes
{
    public class JQMenuAttribute : Attribute
    {
        public string DataAttribute { get; set; }
    }
}
