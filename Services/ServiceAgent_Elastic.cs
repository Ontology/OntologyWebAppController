﻿using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntologyWebAppController.BusinessModels;
using OntologyWebAppController.Notifications;
using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OntologyWebAppController.Services
{
    public class ServiceAgent_Elastic : NotifyPropertyChange
    {
        private clsLocalConfig localConfig;
        private OntologyModDBConnector dbReader_ViewToController;
        private OntologyModDBConnector dbReader_ViewToModule;
        private OntologyModDBConnector dbReader_ModuleToModuleFunction;
        private OntologyModDBConnector dbReader_ViewToCommandlineRun;
        private OntologyModDBConnector dbReader_EntryView;
        private OntologyModDBConnector dbReader_OItems;

        private clsOntologyItem oItemRef_Security;

        private System.Threading.Thread threadSecItems;

        private ElasticSearchNestConnector.clsUserAppDBSelector userDBSelector;
        public ElasticSearchNestConnector.clsUserAppDBSelector UserDbSelector
        {
            get { return userDBSelector; }
        }

        private ElasticSearchNestConnector.clsUserAppDBUpdater userDBUpdater;
        public ElasticSearchNestConnector.clsUserAppDBUpdater UserDbUpdater
        {
            get { return userDBUpdater; }
        }

        private clsOntologyItem resultData;
        public clsOntologyItem ResultData
        {
            get { return resultData; }
            set
            {
                resultData = value;
                RaisePropertyChanged(NotifyChanges.Elastic_ResultData);
            }
        }

        private clsOntologyItem resultPasswordList;
        public clsOntologyItem ResultPasswordList
        {
            get
            {
                return resultPasswordList;
            }
            set
            {
                resultPasswordList = value;
                RaisePropertyChanged(NotifyChanges.Elastic_ResultPasswordList);
            }
        }

        public List<clsObjectRel> ViewsToController
        {
            get { return dbReader_ViewToController.ObjectRels; }
        }

        public List<clsObjectRel> ViewsToModules
        {
            get { return dbReader_ViewToModule.ObjectRels; }
        }

        public List<clsObjectRel> ModulesToModuleFunctions
        {
            get { return dbReader_ModuleToModuleFunction.ObjectRels; }
        }

        public List<clsObjectRel> ViewsToCommandLineRun
        {
            get { return dbReader_ViewToCommandlineRun.ObjectRels; }
        }

        private Thread thread_GetData;

        public clsOntologyItem GetData()
        {
            try
            {
                if (thread_GetData != null)
                {
                    thread_GetData.Abort();
                }
            }
            catch (Exception ex) { }

            thread_GetData = new Thread(GetDataAsync);
            thread_GetData.Start();

            return localConfig.Globals.LState_Success.Clone();
        }

        private void GetDataAsync()
        {
            var result = GetSubData_001_ViewToController();

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                result = GetSubData_002_ViewToModule();
            }

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                result = GetSubData_003_ModuleToModuleFunction();
            }

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                result = GetSubData_004_ViewToCommandlineRun();
            }

            ResultData = result;
        }

        private clsOntologyItem GetSubData_001_ViewToController()
        {
            var searchViews = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Parent_Object = localConfig.OItem_class_websocketview.GUID,
                    ID_RelationType = localConfig.OItem_relationtype_belongs_to.GUID,
                    ID_Parent_Other = localConfig.OItem_class_websocketcontroller.GUID
                }
            };

            var result = dbReader_ViewToController.GetDataObjectRel(searchViews);

            return result;
        }

        private clsOntologyItem GetSubData_002_ViewToModule()
        {
            var searchViews = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Parent_Object = localConfig.OItem_class_websocketview.GUID,
                    ID_RelationType = localConfig.OItem_relationtype_belongs_to.GUID,
                    ID_Parent_Other = localConfig.OItem_class_module.GUID
                }
            };

            var result = dbReader_ViewToModule.GetDataObjectRel(searchViews);

            return result;
        }

        private clsOntologyItem GetSubData_003_ModuleToModuleFunction()
        {
            var searchModuleFunction = dbReader_ViewToModule.ObjectRels.Select(moduleItem => new clsObjectRel
            {
                ID_Object = moduleItem.ID_Other,
                ID_RelationType = localConfig.OItem_relationtype_is_of_type.GUID,
                ID_Parent_Other = localConfig.OItem_class_module_function.GUID
            }).ToList();

            var result = localConfig.Globals.LState_Success.Clone();
            if (searchModuleFunction.Any())
            {
                result = dbReader_ModuleToModuleFunction.GetDataObjectRel(searchModuleFunction);
            }
            else
            {
                dbReader_ModuleToModuleFunction.ObjectRels.Clear();
            }
            

            return result;
        }

        private clsOntologyItem GetSubData_004_ViewToCommandlineRun()
        {
            var searchCMDLRs = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Parent_Object = localConfig.OItem_class_websocketview.GUID,
                    ID_RelationType = localConfig.OItem_relationtype_offers.GUID,
                    ID_Parent_Other = localConfig.OItem_class_comand_line__run_.GUID
                }
            };

            var result = dbReader_ViewToCommandlineRun.GetDataObjectRel(searchCMDLRs);

            return result;
        }

        public List<clsObjectRel> GetEntryViewData(string idEntryView)
        {
            var searchEntryViewRel = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Object = idEntryView,
                    ID_RelationType = localConfig.OItem_relationtype_root_resource.GUID
                }
            };

            var result = dbReader_EntryView.GetDataObjectRel(searchEntryViewRel);

            if (result.GUID == localConfig.Globals.LState_Error.GUID) return null;

            return dbReader_EntryView.ObjectRels;
        }

        public void GetSecuredItems(clsOntologyItem oItemRef)
        {
            oItemRef_Security = oItemRef;

            if (threadSecItems != null)
            {
                try
                {

                }
                catch (Exception ex)
                {
                    threadSecItems.Abort();
                }
                

            }

            threadSecItems = new Thread(GetSecuredItemsAsync);
            threadSecItems.Start();
        }

        public void GetSecuredItemsAsync()
        {
           
        }

        public clsOntologyItem GetOItem(string id, string type)
        {
            return dbReader_OItems.GetOItem(id, type);
        }
        public ServiceAgent_Elastic(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;

            Initialize();
        }

        private void Initialize()
        {
            dbReader_ViewToController = new OntologyModDBConnector(localConfig.Globals);
            dbReader_ViewToModule = new OntologyModDBConnector(localConfig.Globals);
            dbReader_ModuleToModuleFunction = new OntologyModDBConnector(localConfig.Globals);
            dbReader_ViewToCommandlineRun = new OntologyModDBConnector(localConfig.Globals);
            dbReader_EntryView = new OntologyModDBConnector(localConfig.Globals);
            dbReader_OItems = new OntologyModDBConnector(localConfig.Globals);

            userDBSelector = new ElasticSearchNestConnector.clsUserAppDBSelector(localConfig.Globals.Server, localConfig.Globals.Port, "sessiondata", 5000, localConfig.Globals.Session);
            userDBUpdater = new ElasticSearchNestConnector.clsUserAppDBUpdater(userDBSelector);
        }
    }
}
